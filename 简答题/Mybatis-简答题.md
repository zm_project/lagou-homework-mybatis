#一、简答题

### 1.Mybatis动态sql是做什么的？都有哪些动态sql？简述一下动态sql的执行原理？

##### 1.动态SQL的概念

 动态sql是指在进行sql操作的时候，传入的参数对象或者参数值，根据匹配的条件，有需要的动态去判断是否为空，循环，拼接等情况；

##### 2.动态Sql的标签大致有以下几种

###### if 和 where 标签和include标签

 if标签中可以判断传入的值是否符合某种规则，比如是否不为空；

 where标签可以用来做动态拼接查询条件，当和if标签配合的时候，不用显示的声明类似where 1=1这种无用的条件，来达到匹配的时候and会多余的情况；

 include可以把大量重复的代码整理起来，当使用的时候直接include即可，减少重复代码的编写

```xml
<!--动态Sql : where / if-->
<select id="findUserById"  resultType="com.lagou.pojo.User">
  select <include refid="userInfo"/> from user
  <where>
    <if test="id != null and id != 0">
      AND id = #{id}
    </if>
    <if test="name != null and name != ''">
      AND name = #{name}
    </if>
  </where>
</select>
```

###### choose、when、otherwise 标签

 类似于 Java 中的 switch、case、default。只有一个条件生效，也就是只执行满足的条件 when，没有满足的条件就执行 otherwise，表示默认条件

```xml
<!--动态Sql: choose、when、otherwise 标签-->
<select id="findUserById" resultType="com.lagou.pojo.User">
  select * from user
  <where>
    <choose>
      <when test="name != null and name != ''">
        AND name = #{name}
      </when>
      <otherwise>
        AND id = #{id}
      </otherwise>
    </choose>
  </where>
</select>
```



###### foreach 标签

 foreach标签可以把传入的集合对象进行遍历，然后把每一项的内容作为参数传到sql语句中，里面涉及到 **item**(具体的每一个对象), **index**(序号), **open**(开始符), **close**(结束符), **separator**(分隔符)

```xml
<!--动态Sql: foreach标签, 批量插入-->
<insert id="insertBatch" useGeneratedKeys="true" keyProperty="id">
  insert into user (id, name)
  values
  <foreach collection="list" item="user" separator="," >
    (#{user.id}, #{user.name})
  </foreach>
</insert>

<!--动态Sql: foreach标签, in查询-->
<select id="dynamicSqlSelectList" resultType="com.lagou.pojo.User">
  SELECT * from user WHERE id in
  <foreach collection="list" item="id" open="(" close=")" separator="," >
    #{id}
  </foreach>
</select>
```



###### map参数

 < map> 标签需要结合MyBatis的参数注解 @Param()来使用，需要告诉Mybatis配置文件中的collection="map"里的map是一个参数

```xml
<!--动态Sql: foreach标签, map参数查询-->
<select id="findByMap" resultType="com.lagou.pojo.User">
  select * from user WHERE
  <foreach collection="map" index="key" item="value"  separator="=">
    ${key} = #{value}
  </foreach>
</select>
1234567
```



###### set标签

 适用于更新中，当匹配某个条件后，才会对该字段进行更新操作

```xml
<!--动态Sql: set 标签-->
<update id="updateSet" parameterType="com.lagou.pojo.User">
  update user
  <set>
    <if test="name != null and name != ''">
      name = #{name},
    </if>
  </set>
  where id = #{id}
</update>
12345678910
```

###### trim标签

是一个格式化标签，主要有4个参数:

**prefix**(前缀)

**prefixOverrides**(去掉第一个标记)

**suffix**(后缀)

**suffixOverrides**(去掉最后一个标记)

```xml
<!--动态Sql: trim 标签-->
<select id="findUser" resultType="com.lagou.pojo.User">
  select * from user
  <trim prefix="where" suffix="order by id" prefixOverrides="and | or" suffixOverrides=",">
    <if test="name != null and name != ''">
      AND name = #{name}
    </if>
    <if test="id != null">
      AND id = #{id}
    </if>
  </trim>
</select>
123456789101112
```

##### 3.动态sql的执行原理

- 首先在解析xml配置文件的时候，会有一个SqlSource sqlSource = langDriver.createSqlSource(configuration, context, parameterTypeClass) 的操作
- createSqlSource底层使用了XMLScriptBuilder来对xml中的标签进行解析
- XMLScriptBuilder调用了parseScriptNode()的方法，
- 在parseScriptNode()的方法中有一个parseDynamicTags()方法，会对nodeHandlers里的标签根据不同的handler来处理不同的标签
- 然后把DynamicContext结果放回SqlSource中
- DynamicSqlSource获取BoundSql
- 在Executor执行的时候，调用DynamicSqlSource的解析方法，并返回解析好的BoundSql，和已经排好序，需要替换的参数
  



 简单的说：就是使用OGNL从sql参数对象中计算表达式的值，根据表达式的值动态拼接sql



### 2.Mybatis是否支持延迟加载？如果支持，它的实现原理是什么？

Mybatis 仅支持 association 关联对象和 collection 关联集合对象的延迟加

载，association 指的就是一对一，collection 指的就是一对多查询。在 Mybatis

配置文件中，可以配置是否启用延迟加载 lazyLoadingEnabled=true|false。

它的原理是，使用 CGLIB 创建目标对象的代理对象，当调用目标方法时，进入拦

截器方法，比如调用 a.getB().getName()，拦截器 invoke()方法发现 a.getB()是

null 值，那么就会单独发送事先保存好的查询关联 B 对象的 sql，把 B 查询上来，

然后调用 a.setB(b)，于是 a 的对象 b 属性就有值了，接着完成 a.getB().getName()

方法的调用。这就是延迟加载的基本原理。

当然了，不光是 Mybatis，几乎所有的包括 Hibernate，支持延迟加载的原理都

是一样的。



### 3.Mybatis都有哪些Executor执行器？它们之间的区别是什么？

**1、SimpleExecutor**：每执行一次update或select，就开启一个Statement对象，用完立刻关闭Statement对象。

**2、ReuseExecutor**：执行update或select，以sql作为key查找Statement对象，存在就使用，不存在就创建，用完后，不关闭Statement对象，而是放置于Map内，供下一次使用。简言之，就是重复使用Statement对象。

**3、BatchExecutor**：执行update（没有select，JDBC批处理不支持select），将所有sql都添加到批处理中（addBatch()），等待统一执行（executeBatch()），它缓存了多个Statement对象，每个Statement对象都是addBatch()完毕后，等待逐一执行executeBatch()批处理。与JDBC批处理相同。



**作用范围**：Executor的这些特点，都严格限制在SqlSession生命周期范围内。

默认是`SimplExcutor`，需要配置在创建SqlSession对象的时候指定执行器的类型即可。



### 4.简述下Mybatis的一级、二级缓存（分别从存储结构、范围、失效场景。三个方面来作答）？

1. **一级缓存**：

Mybatis的一级缓存是指SqlSession级别的，作用域是SqlSession，Mybatis默认开启一级缓存，在同一个SqlSession中，相同的Sql查询的时候，第一次查询的时候，就会从缓存中取，如果发现没有数据，那么就从数据库查询出来，并且缓存到HashMap中，如果下次还是相同的查询，就直接从缓存中查询，就不在去查询数据库，对应的就不在去执行SQL语句。当查询到的数据，进行增删改的操作的时候，缓存将会失效。在spring容器管理中每次查询都是创建一个新的sqlSession，所以在分布式环境中不会出现数据不一致的问题.

 

2. **二级缓存**：

二级缓存是mapper级别的缓存，多个SqlSession去操作同一个mapper的sql语句，多个SqlSession可以共用二级缓存，二级缓存是跨SqlSession。第一次调用mapper下的sql 的时候去查询信息，查询到的信息会存放到该mapper对应的二级缓存区域，第二次调用namespace下的mapper映射文件中，相同的SQL去查询，回去对应的二级缓存内取结果，使用值需要开启cache标签，在select上添加useCache属性为true，在更新和删除时候需要手动开启flushCache刷新缓存。



### 5.简述Mybatis的插件运行原理，以及如何编写一个插件？

Mybatis插件其实就是拦截器，Mybatis仅可以编写针对ParameterHandler、ResultSetHandler、StatementHandler、Executor这4大核心对象的插件，Mybatis使用JDK的动态代理，为需要拦截的接口生成代理对象以实现接口方法拦截功能，每当执行这4种接口对象的方法时，就会进入拦截方法，具体就是InvocationHandler的invoke()方法，当然，只会拦截那些你指定需要拦截的方法。

**编写插件**：实现Mybatis的Interceptor接口并复写intercept()方法，然后在给插件编写注解，指定要拦截哪一个接口的哪些方法即可，记住，别忘了在配置文件中配置你编写的插件。