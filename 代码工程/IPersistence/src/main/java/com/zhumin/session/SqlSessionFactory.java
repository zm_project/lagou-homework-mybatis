package com.zhumin.session;

/**
 * @author zhumin
 **/
public interface SqlSessionFactory {

    /**
     * 打开sqlSession会话
     *
     * @return
     */
    SqlSession openSession();
}
