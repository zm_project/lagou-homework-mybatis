package com.zhumin.session;

import java.util.List;

/**
 * @author zhumin
 **/
public interface SqlSession {

    /**
     * 查询所有
     *
     * @param statementId
     * @param params
     * @param <E>
     * @return
     * @throws Exception
     */
    <E> List<E> selectList(String statementId, Object... params) throws Exception;

    /**
     * 查询单个
     *
     * @param statementId
     * @param params
     * @param <T>
     * @return
     * @throws Exception
     */
    <T> T selectOne(String statementId, Object... params) throws Exception;

    /**
     * 为Dao接口生产代理实现类
     *
     * @param mapperClass
     * @param <T>
     * @return
     */
    <T> T getMapper(Class<T> mapperClass);

    /**
     * 添加
     * @param statementId
     * @param args
     * @return
     * @throws Exception
     */
    boolean insert(String statementId, Object... args) throws Exception;

    /**
     * 更新
     * @param statementId
     * @param args
     * @return
     * @throws Exception
     */
    boolean update(String statementId, Object... args) throws Exception;

    /**
     * 删除
     * @param statementId
     * @param args
     * @return
     * @throws Exception
     */
    boolean delete(String statementId, Object... args) throws Exception;

}
