package com.zhumin.session;

import com.zhumin.pojo.Configuration;
import com.zhumin.pojo.MappedStatement;

import java.lang.reflect.*;
import java.util.List;

/**
 * @author zhumin
 **/
public class DefaultSqlSession implements SqlSession {

    private Configuration configuration;

    public DefaultSqlSession(Configuration configuration) {
        this.configuration = configuration;
    }


    @Override
    public <E> List<E> selectList(String statementId, Object... params) throws Exception {
        // 将要去完成对SimpleExecutor里的query方法的调用
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        List<Object> list = simpleExecutor.query(configuration, mappedStatement, params);

        return (List<E>) list;
    }

    @Override
    public <T> T selectOne(String statementId, Object... params) throws Exception {
        List<Object> objects = selectList(statementId, params);
        if (objects.size() == 1) {
            return (T) objects.get(0);
        }else {
            throw new RuntimeException("查询结果为空或者返回结果过多");
        }
    }

    @Override
    public <T> T getMapper(Class<T> mapperClass) {
        // 使用JDK动态代理来为Dao接口生产代理对象，并返回
        Object proxyInstance = Proxy.newProxyInstance(DefaultSqlSession.class.getClassLoader(), new Class[]{mapperClass}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                // 底层都还是去执行JDBC代码
                // 根据不同情况，来调用操作
                // 准备参数1： statementId ：sql语句的唯一标识（namespace.id=接口全限定名.方法名）
                String methodName = method.getName();
                String className = method.getDeclaringClass().getName();

                String statementId = className + "." + methodName;

                // 准备参数2：params：args

                MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
                if (mappedStatement == null) {
                    throw new RuntimeException("mappedStatement is null");
                }

                String type = mappedStatement.getType();
                switch (type) {
                    case "select":
                        // 获取被调用方法的返回值类型
                        Type genericReturnType = method.getGenericReturnType();
                        // 判断是否进行了 泛型参数序列化
                        if (genericReturnType instanceof ParameterizedType) {
                            return selectList(statementId, args);
                        }

                        return selectOne(statementId, args);
                    case "insert":
                        return insert(statementId, args);
                    case "delete":
                        return delete(statementId, args);
                    case "update":
                        return update(statementId, args);
                    default:
                        throw new RuntimeException("unknown execution method");
                }
            }
        });

        return (T) proxyInstance;
    }

    @Override
    public boolean insert(String statementId, Object... args) throws Exception {
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        return simpleExecutor.insert(configuration, mappedStatement, args);
    }

    @Override
    public boolean update(String statementId, Object... args) throws Exception {
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        return simpleExecutor.update(configuration, mappedStatement, args);
    }

    @Override
    public boolean delete(String statementId, Object... args) throws Exception {
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        return simpleExecutor.delete(configuration, mappedStatement, args);
    }
}
