package com.zhumin.session;

import com.zhumin.config.XMLConfigBuilder;
import com.zhumin.pojo.Configuration;
import org.dom4j.DocumentException;

import java.beans.PropertyVetoException;
import java.io.InputStream;

/**
 * sqlSessionFactory 构建者
 *
 * @author zhumin
 **/
public class SqlSessionFactoryBuilder {

    /**
     * 构建 sqlSessionFactory 会话工厂对象
     * @param in xml配置文件流
     * @return
     */
    public SqlSessionFactory build(InputStream in) throws DocumentException, PropertyVetoException {
        // 第一步：使用dom4j解析配置文件，将解析出来的文件内容封装到configuration中
        XMLConfigBuilder xmlConfigBuilder = new XMLConfigBuilder();
        Configuration configuration = xmlConfigBuilder.parseConfig(in);

        // 第二步：创建sqlSessionFactory对象，工厂类：生产sqlSession：会话对象

        DefaultSqlSessionFactory defaultSqlSessionFactory = new DefaultSqlSessionFactory(configuration);
        return defaultSqlSessionFactory;
    }
}
