package com.zhumin.session;

import com.zhumin.pojo.Configuration;
import com.zhumin.pojo.MappedStatement;

import java.util.List;

/**
 * @author zhumin
 **/
public interface Executor {

    <E> List<E> query(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception;

    boolean insert(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception;

    boolean update(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception;

    boolean delete(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception;

}
