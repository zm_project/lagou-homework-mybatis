package com.zhumin.utils;

/**
 * @author zhumin
 **/
public interface TokenHandler {

    /**
     *
     * @param content 参数名称
     * @return
     */
    String handleToken(String content);
}
