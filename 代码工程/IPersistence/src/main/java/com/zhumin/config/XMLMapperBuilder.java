package com.zhumin.config;

import com.zhumin.pojo.Configuration;
import com.zhumin.pojo.MappedStatement;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * 解析所有mapper配置并封装到 configuration的 mappedStatementMap 中
 *
 * @author zhumin
 **/
public class XMLMapperBuilder {

    private Configuration configuration;

    public XMLMapperBuilder(Configuration configuration) {
        this.configuration = configuration;
    }

    public void parse(InputStream in) throws DocumentException {
        Document document = new SAXReader().read(in);
        Element rootElement = document.getRootElement();

        String namespace = rootElement.attributeValue("namespace");

        // 解析 <select /> <insert /> <update /> <delete /> 节点们
        buildStatement(rootElement, namespace, Arrays.asList("select", "insert", "update", "delete"));
    }

    private void buildStatement(Element rootElement, String namespace, List<String> nodes) {
        if (nodes == null || nodes.isEmpty()) {
            return;
        }

        nodes.forEach(node -> {
            List<Element> list = rootElement.selectNodes("//" + node);
            list.forEach(element -> {
                String id = element.attributeValue("id");
                String resultType = element.attributeValue("resultType");
                String parameterType = element.attributeValue("parameterType");
                String sql = element.getTextTrim();
                MappedStatement mappedStatement = new MappedStatement();
                mappedStatement.setId(id);
                mappedStatement.setResultType(resultType);
                mappedStatement.setParameterType(parameterType);
                mappedStatement.setSql(sql);
                mappedStatement.setType(node);

                String key = namespace + "." + id;

                configuration.getMappedStatementMap().put(key, mappedStatement);
            });
        });

    }
}
