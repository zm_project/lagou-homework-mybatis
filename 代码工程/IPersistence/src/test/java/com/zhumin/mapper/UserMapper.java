package com.zhumin.mapper;

import com.zhumin.pojo.User;

import java.util.List;

/**
 * @author zhumin
 **/
public interface UserMapper {

    List<User> findAll();

    User findByCondition(User user);

    boolean insert(User user);

    boolean update(User user);

    boolean delete(User user);

}
