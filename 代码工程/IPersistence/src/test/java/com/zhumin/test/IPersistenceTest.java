package com.zhumin.test;

import com.zhumin.io.Resources;
import com.zhumin.mapper.UserMapper;
import com.zhumin.pojo.User;
import com.zhumin.session.SqlSession;
import com.zhumin.session.SqlSessionFactory;
import com.zhumin.session.SqlSessionFactoryBuilder;
import org.dom4j.DocumentException;
import org.junit.Test;

import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.util.List;

/**
 * @author zhumin
 **/
public class IPersistenceTest {

    @Test
    public void test() throws PropertyVetoException, DocumentException {
        InputStream resourceAsStream = Resources.getResourceAsSteam("SqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();

        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        // 现在先来添加一个新的用户
//        boolean status = userMapper.insert(new User(2, "lagou"));

        // 查询单个用户
//        User user1 = userMapper.findByCondition(new User(2, null));
//        System.out.println(user1);

        // 更新用户信息
//        boolean updateStatus = userMapper.update(new User(2, "lagou-new"));

//        User user2 = userMapper.findByCondition(new User(2, null));
//        System.out.println(user2);

        // 删除指定用户
        boolean delete = userMapper.delete(new User(2, null));

        // 查询所有用户
        List<User> all = userMapper.findAll();
        all.forEach(user -> {
            System.out.println(user);
        });


    }

}
